data BaseNucleotidica = A | C | G | T | U deriving Show
type CadenaDNA = [BaseNucleotidica]
type CadenaRNA = [BaseNucleotidica]
type Codon =(BaseNucleotidica, BaseNucleotidica, BaseNucleotidica)
data Aminoacido = Phe | Ser | Tyr | Cys | Leu | Trp | Pro | His | Arg | Gln | Ile | Thr | Asn | Lys | Met | Val | Ala | Asp | Gly | Glu deriving Show
type Proteina = [Aminoacido]

complementarBase :: BaseNucleotidica -> BaseNucleotidica
complementarBase A = T
complementarBase T = A
complementarBase C = G
complementarBase G = C

complementarCadenaDNA :: CadenaDNA -> CadenaDNA
complementarCadenaDNA [] = []
complementarCadenaDNA (x:xs) = (complementarBase x) : complementarCadenaDNA xs

obtenerCadenaReverseDNA :: CadenaDNA -> CadenaDNA
obtenerCadenaReverseDNA [] = []
obtenerCadenaReverseDNA (x:xs) = obtenerCadenaReverseDNA xs ++ [x]

transcribir :: CadenaDNA -> CadenaDNA
transcribir [] = []
transcribir (T:xs) =  (U : transcribir xs)
transcribir (x:xs) =   x : transcribir xs 

traducirCodonAAminoacido :: Codon -> Aminoacido
traducirCodonAAminoacido (U,U,_) = Phe
traducirCodonAAminoacido (U,C,_) = Ser
traducirCodonAAminoacido (U,A,U) = Tyr
traducirCodonAAminoacido (U,A,C) = Tyr
traducirCodonAAminoacido (U,G,U) = Cys
traducirCodonAAminoacido (U,G,C) = Cys
traducirCodonAAminoacido (U,G,G) = Trp
traducirCodonAAminoacido (C,U,_) = Leu
traducirCodonAAminoacido (C,C,_) = Pro
traducirCodonAAminoacido (C,A,U) = His
traducirCodonAAminoacido (C,A,C) = His
traducirCodonAAminoacido (C,A,A) = Gln
traducirCodonAAminoacido (C,A,G) = Gln
traducirCodonAAminoacido (C,G,_) = Arg
traducirCodonAAminoacido (A,U,U) = Ile
traducirCodonAAminoacido (A,U,C) = Ile
traducirCodonAAminoacido (A,U,A) = Ile
traducirCodonAAminoacido (A,U,G) = Met
traducirCodonAAminoacido (A,C,_) = Thr
traducirCodonAAminoacido (A,A,U) = Asn
traducirCodonAAminoacido (A,A,C) = Asn
traducirCodonAAminoacido (A,A,A) = Lys
traducirCodonAAminoacido (A,A,G) = Lys
traducirCodonAAminoacido (A,G,U) = Ser
traducirCodonAAminoacido (A,G,C) = Ser
traducirCodonAAminoacido (A,G,A) = Arg
traducirCodonAAminoacido (A,G,G) = Arg
traducirCodonAAminoacido (G,U,_) = Val
traducirCodonAAminoacido (G,C,_) = Ala
traducirCodonAAminoacido (G,G,_) = Gly
traducirCodonAAminoacido (G,A,U) = Asp
traducirCodonAAminoacido (G,A,C) = Asp
traducirCodonAAminoacido (G,A,A) = Glu
traducirCodonAAminoacido (G,A,G) = Glu


obtenerProteinasDeRNA :: CadenaRNA -> [Proteina]
obtenerProteinasDeRNA [] = []
obtenerProteinasDeRNA (x:y:z:xs) = [[traducirCodonAAminoacido (x,y,z)] ++ [obtenerProteinas xs]]

--sincronizaConCodonDeFin :: CadenaRNA -> Bool
--sincronizaConCodonDeFin [] = False 

buscoAUG :: CadenaDNA -> Bool
buscoAUG [] = False
buscoAUG (A,U,G,xs) = True
buscoAUG (x:y:z:xs) = buscoAUG(y:z:xs)
buscoAUG (_: _: z) = False
buscoAUG (_: y) = False

buscoPosicionAUG :: CadenaDNA -> Bool


--obtenerProteina :: CadenaDNA -> [Proteina]
--obtenerProteina 








